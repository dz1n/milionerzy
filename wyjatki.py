class Error(Exception):
   def __init__(self):
       Exception.__init__(self)

class Wyjatek(Error):
    def __init__(self,var1):
        self.pieniadze = var1

def SprawdzCzyMaszWystarczającąLiczbeGotówki(pieniadze,var1):
    if pieniadze < var1:
        raise Wyjatek(pieniadze)
    else:
        return True