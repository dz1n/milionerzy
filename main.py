import pygame, pygame.gfxdraw
import time
import random
import os
import wyjatki

game_state = 0

class Zapisywanie_w_pliku:
    def zapisz_dane(self,plik,x):
        dane = open(str(plik),'w')
        dane.write(x)
        dane.flush()

    def zapisz_dane_w_linicje(self,plik,x,linijka):
        if Zapisywanie_w_pliku().czy_plik_posiada_dane(str(plik)) == False:
            Zapisywanie_w_pliku().zapisz_dane(str(plik),x)
        else:
            with open("savefile.txt") as f:
                linie = f.readlines()
            linie[linijka] = str(x) + "\n"
            with open("savefile.txt", "w") as f:
                f.writelines(linie)  # write back

    def odczytaj_dane(self,plik):
        dane = open(str(plik), 'r')
        #dane.seek(0)
        x = dane.read()
        dane.seek(0)
        return x

    def odczytaj_podane_dane(self,plik,x):
        p = 0
        dane_zwr = ""
        dane = open(str(plik), 'r')
        for r in dane:
            p+=1
            if p == x:
                dane_zwr = r
                break
        dane.seek(0)
        return dane_zwr

    def czy_plik_posiada_dane(self,plik):
        if os.path.getsize(str(plik)) > 0:
            return True
        return False


class Kasyno:
    def __init__(self):
        self.nazwa_gracza = ''
        self.pieniadze = 0
    def odczczytaj_dane(self):
        with open('savefile.txt') as f:
            nazwa = f.readline()
            for line in f:
                pass
            kasa = line
            f.seek(0)
        return [nazwa,kasa]
    def Vegas(self):
        Menu = False
        Opcja_w_menu = 0
        x = Zapisywanie_w_pliku().odczytaj_dane("savefile.txt")
        xy = ""
        for y in x:
            if y == "\n":
                break
            xy += y
        nazwa = xy

        pieniadze = int(Zapisywanie_w_pliku().odczytaj_podane_dane("savefile.txt",2))
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                mouse = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if (mouse[0] > 251 and mouse[0] < 848) and (mouse[1]>301 and mouse[1]<497):
                        Kasyno().CoinFlip(pieniadze,nazwa)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        if Menu:
                            Menu = False
                        else:
                            Menu = True
                    if event.key == pygame.K_UP:
                        Opcja_w_menu += 1
                    if event.key == pygame.K_DOWN:
                        Opcja_w_menu -= 1
                    if event.key == pygame.K_RETURN:
                        if Opcja_w_menu == 0:
                            quit()
                        if Opcja_w_menu == -1:
                            Milionerzy().gra(nazwa)

            display.blit(kasyno_menu_tlo, (0, 0))

            pygame.draw.rect(display, black, pygame.Rect(250, 300, 600, 200))
            pygame.draw.rect(display, black, pygame.Rect(250, 700, 600, 200))
            pygame.draw.rect(display, black, pygame.Rect(1150, 300, 600, 200))
            pygame.draw.rect(display, black, pygame.Rect(1150, 700, 600, 200))
            display.blit(menu_font.render("Rzut monetą", True, white), [255, 355])
            display.blit(menu_font.render("Niedostępne", True, red), [255, 755])
            display.blit(menu_font.render("Niedostępne", True, red), [1155, 355])
            display.blit(menu_font.render("Niedostępne", True, red), [1155, 755])

            pygame.draw.rect(display, (255, 53, 0), pygame.Rect(20, 50, 400, 150))
            display.blit(odpowiedz_font.render("Kliknij ESC by", True, white), [20, 75])
            display.blit(odpowiedz_font.render("wejść do menu", True, white), [20, 125])
            if Menu:
                if Opcja_w_menu > 0:
                    Opcja_w_menu = 0
                if Opcja_w_menu < -1:
                    Opcja_w_menu = -1
                pygame.gfxdraw.box(display, pygame.Rect(565, 200, 800, 600), (200, 200, 200,227))
                display.blit(odpowiedz_font.render("Menu", True, (255,255,0)), [570, 205])
                pygame.draw.rect(display, (255, 53, 0), pygame.Rect(765, 300, 400, 150))
                pygame.draw.rect(display, (255, 53, 0), pygame.Rect(765, 550, 400, 150))
                if Opcja_w_menu == 0:
                    pygame.draw.rect(display, black, pygame.Rect(765, 300, 400, 150),10)
                if Opcja_w_menu == -1:
                    pygame.draw.rect(display, black, pygame.Rect(765, 550, 400, 150),10)
                display.blit(odpowiedz_font.render("Wyjdz z gry", True, white), [770, 320])
                display.blit(odpowiedz_font.render("Przejdz do", True, white), [770, 570])
                display.blit(odpowiedz_font.render("Milionerów", True, white), [770, 620])

            pygame.display.update()

    def CoinFlip(self,pieniadze,nazwa):
        Menu = False
        Opcja_w_menu = 0
        wygrane = 0
        postawiona_kasa = 1000
        animacja = 0
        coinflip_text = ""
        err_text = ""
        coin = pygame.image.load('images/Rzut_monetą/7.png')
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                mouse = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    try:
                      if wyjatki.SprawdzCzyMaszWystarczającąLiczbeGotówki(pieniadze,999):
                        err_text = ""
                        if (mouse[0] > 801 and mouse[0] < 1050) and (mouse[1] > 501 and mouse[1] < 750):
                            animacja = 700
                    except wyjatki.Wyjatek as f:
                        err_text = "Brakuje ci jeszcze " + str(1000 - pieniadze) + "zł by w to zagrać"
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        if Menu:
                            Menu = False
                        else:
                            Menu = True
                    if event.key == pygame.K_UP:
                        Opcja_w_menu += 1
                    if event.key == pygame.K_DOWN:
                        Opcja_w_menu -= 1
                    if event.key == pygame.K_RETURN:
                        if Opcja_w_menu == 0:
                            quit()
                        if Opcja_w_menu == -1:
                            x = Zapisywanie_w_pliku().odczytaj_dane("savefile.txt")
                            xy = ""
                            for y in x:
                                if y == "\n":
                                    break
                                xy += y
                            Milionerzy().gra(xy)
                    if event.key == pygame.K_RIGHT:
                        if postawiona_kasa < pieniadze:
                            if postawiona_kasa > 10000:
                                postawiona_kasa += 10000
                            else:
                                postawiona_kasa += 1000
                    if event.key == pygame.K_LEFT:
                        if postawiona_kasa > 1001:
                            if postawiona_kasa > 1000:
                                postawiona_kasa -= 4000
                            else:
                                postawiona_kasa -= 1000

            display.blit(coinflip_tlo, (0, 0))
            pygame.draw.rect(display, (255, 53, 0), pygame.Rect(800, 500, 250, 250))
            if animacja > 0:
                animacja -= 10
                if animacja == 700:
                    coin = pygame.image.load('images/Rzut_monetą/7.png')
                if animacja == 600:
                    coin = pygame.image.load('images/Rzut_monetą/6.png')
                if animacja == 500:
                    coin = pygame.image.load('images/Rzut_monetą/5.png')
                if animacja == 400:
                    coin = pygame.image.load('images/Rzut_monetą/4.png')
                if animacja == 300:
                    coin = pygame.image.load('images/Rzut_monetą/3.png')
                if animacja == 200:
                    coin = pygame.image.load('images/Rzut_monetą/2.png')
                if animacja == 100:
                    coin = pygame.image.load('images/Rzut_monetą/1.png')

                if animacja == 10:
                    try:
                        coin = random.randint(0, 1000)
                    except ValueError:
                        print("[Debug] Losowanie sie popsuło")
                    if coin > 490 + (wygrane * 20):
                        wygrane += 1
                        postawiona_kasa = postawiona_kasa * 1.8
                        coinflip_text = "Wygrałeś!!"
                    else:
                        wygrane = 0
                        postawiona_kasa = postawiona_kasa - (postawiona_kasa * 2)
                        coinflip_text = "Przegrałeś :("
                    Zapisywanie_w_pliku().zapisz_dane_w_linicje("savefile.txt", str(int(int(Zapisywanie_w_pliku().odczytaj_podane_dane("savefile.txt", 2)) + postawiona_kasa)), 1)
                    pieniadze += postawiona_kasa
                    postawiona_kasa = 1000
                    coin = pygame.image.load('images/Rzut_monetą/7.png')
            display.blit(coin, (805, 504))
            display.blit(odpowiedz_font.render(coinflip_text, True, white), [775, 440])
            display.blit(odpowiedz_font.render(err_text, True, white), [400, 760])
            display.blit(odpowiedz_font.render(nazwa+" "+str(pieniadze)+"zł", True, white), [1300, 150])
            display.blit(odpowiedz_font.render("Stawiasz: "+str(postawiona_kasa)+"zł", True, white), [1300, 350])
            pygame.draw.rect(display, (255, 53, 0), pygame.Rect(20, 50, 400, 150))
            display.blit(odpowiedz_font.render("Kliknij ESC by", True, white), [20, 75])
            display.blit(odpowiedz_font.render("wejść do menu", True, white), [20, 125])
            if Menu:
                if Opcja_w_menu > 0:
                    Opcja_w_menu = 0
                if Opcja_w_menu < -1:
                    Opcja_w_menu = -1
                pygame.gfxdraw.box(display, pygame.Rect(565, 200, 800, 600), (200, 200, 200, 227))
                display.blit(odpowiedz_font.render("Menu", True, (255, 255, 0)), [570, 205])
                pygame.draw.rect(display, (255, 53, 0), pygame.Rect(765, 300, 400, 150))
                pygame.draw.rect(display, (255, 53, 0), pygame.Rect(765, 550, 400, 150))
                if Opcja_w_menu == 0:
                    pygame.draw.rect(display, black, pygame.Rect(765, 300, 400, 150), 10)
                if Opcja_w_menu == -1:
                    pygame.draw.rect(display, black, pygame.Rect(765, 550, 400, 150), 10)
                display.blit(odpowiedz_font.render("Wyjdz z gry", True, white), [770, 320])
                display.blit(odpowiedz_font.render("Przejdz do", True, white), [770, 570])
                display.blit(odpowiedz_font.render("Milionerów", True, white), [770, 620])

            pygame.display.update()


class Milionerzy:
    def __init__(self):
        self.nazwa_gracza = ""
        self.runda = 0
        self.pieniadze = 0
        self.pytania = []
        self.odpowiedz_1 = []
        self.odpowiedz_2 = []
        self.odpowiedz_3 = []
        self.odpowiedz_4 = []
        self.prawidlowa_odpowiedz = []
        self.debug_mode = False

    def zapisz_dane(self):
        pass
    def odczytaj_dane(self):
        pass
    def menu(self):
        print("[Debug] Menu zostało włączone")
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                mouse = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if (mouse[0] >= 551 and mouse[0] <= 1351) and (mouse[1] >= 490 and mouse[1] <= 591):
                        Milionerzy().Start_Gry()
                        return False


            display.blit(background, (0, 0))
            pygame.draw.rect(display, white, pygame.Rect(550,490,800,100),5)
            pygame.gfxdraw.box(display, pygame.Rect(550,490,800,100), (0, 0, 0, 127))
            display.blit(menu_font.render('Zacznij grę', True, white), [725,500])
            pygame.gfxdraw.box(display, pygame.Rect(1550, 0, 500, 140), (0, 0, 0, 227))
            display.blit(menu_font2.render('Gre Stworzył', True, white), [1560, 20])
            display.blit(menu_font2.render('Kamil Aleksandrowicz', True, white), [1560, 80])
            pygame.display.update()

    def Start_Gry(self):
        print("[Debug] Wpisywanie Nazwy zostało włączone")
        linia = -1
        x = Zapisywanie_w_pliku().odczytaj_dane("savefile.txt")
        xy = ""
        for y in x:
            if y == "\n":
                break
            xy += y
        if Zapisywanie_w_pliku().czy_plik_posiada_dane("savefile.txt"):
            if xy != "":
                Milionerzy().gra(xy)

        self.nazwa_gracza = "" #resetujemy nazwe gracza
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                if event.type == pygame.KEYDOWN:
                    if (len(self.nazwa_gracza)<=20):
                        self.nazwa_gracza+=event.unicode
                    if event.key == pygame.K_BACKSPACE:
                        self.nazwa_gracza = self.nazwa_gracza[:-1]
                    if event.key == pygame.K_RETURN and len(self.nazwa_gracza) > 0:
                        print("[Debug] Nazwa:", self.nazwa_gracza)
                        Zapisywanie_w_pliku().zapisz_dane_w_linicje("savefile.txt",self.nazwa_gracza,0)
                        #savefile_txt.write('\n' + "0")
                        Milionerzy().gra(self.nazwa_gracza)
                        return False
            display.blit(background_gra, (0, 0))
            display.blit(menu_font.render('Podaj swoją nazwę (Maks 20 znaków): ', True, white), [200, 400])
            display.blit(menu_font.render(self.nazwa_gracza, True, white), [900 - (len(self.nazwa_gracza)*20), 500])
            pygame.display.update()

    def gra(self,nazwa_gracza):
        print("[Debug] Gra została włączona")
        #savefile_txt.write('\n'+"0")
        animacja = 0
        text = ''
        odp = ''
        color = (0,0,0)
        koniec = False
        self.nazwa_gracza = nazwa_gracza
        lista = [prawidlowa_odpowiedz, pytania, odpowiedz_1, odpowiedz_2, odpowiedz_3, odpowiedz_4]
        self.prawidlowa_odpowiedz = lista[0]
        self.pytania = lista[1]
        self.odpowiedz_1 = lista[2]
        self.odpowiedz_2 = lista[3]
        self.odpowiedz_3 = lista[4]
        self.odpowiedz_4 = lista[5]
        pytanie = random.randint(0, len(self.pytania) - 1)
        wielkosc = len(self.pytania[pytanie])
        wielkosc1 = len(self.odpowiedz_1[pytanie])
        wielkosc2 = len(self.odpowiedz_2[pytanie])
        wielkosc3 = len(self.odpowiedz_3[pytanie])
        wielkosc4 = len(self.odpowiedz_4[pytanie])
        print("[Debug] Pytania:",self.pytania,"Odpowiedz1: ",self.odpowiedz_1,"Odpowiedz2: ",self.odpowiedz_2,"Odpowiedz3: ", self.odpowiedz_3,"Odpowiedz4: ",self.odpowiedz_4,"Prawidlowa: ",self.prawidlowa_odpowiedz)
        print("[Debug] Pytania zostały wczytane")
        self.runda = 0
        wygrana_gotowka = 0
        linia = -1
        wynik = False
        Menu = False
        Opcja_w_menu = 0
        while True:
            if animacja > 0:
                animacja -= 5
            else:
                text = ""
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_F9:
                        if self.debug_mode:
                            self.debug_mode = False
                        else:
                            self.debug_mode = True
                    if self.debug_mode:
                        if event.key == pygame.K_RIGHT:
                            self.runda += 1
                    if event.key == pygame.K_ESCAPE:
                        #Kasyno().Vegas()
                        if Menu:
                            Menu = False
                        else:
                            Menu = True
                    if event.key == pygame.K_UP:
                        Opcja_w_menu += 1
                    if event.key == pygame.K_DOWN:
                        Opcja_w_menu -= 1
                    if event.key == pygame.K_RETURN:
                        if Opcja_w_menu == 0:
                            quit()
                        if Opcja_w_menu == -1:
                            Kasyno().Vegas()
                    if event.key == pygame.K_1 or event.key == pygame.K_KP1:
                        print("")
                mouse = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEBUTTONDOWN:
                  if animacja <= 0 and Menu == False:
                    odpowiedz = self.prawidlowa_odpowiedz[pytanie]
                    if (mouse[0] >= 459 and mouse[0] <= 954) and (mouse[1] >= 702 and mouse[1] <= 794): #odp1
                        odp = self.odpowiedz_1[pytanie]
                    if (mouse[0] >= 458 and mouse[0] <= 955) and (mouse[1] >= 853 and mouse[1] <= 944): #odp2
                        odp = self.odpowiedz_2[pytanie]
                    if (mouse[0] >= 988  and mouse[0] <= 1484) and (mouse[1] >= 704 and mouse[1] <= 793): #odp3
                        odp = self.odpowiedz_3[pytanie]
                    if (mouse[0] >= 990 and mouse[0] <= 1483) and (mouse[1] >= 853 and mouse[1] <= 942): #odp4
                        odp = self.odpowiedz_4[pytanie]
                    animacja = 1000 + 700
                    if odpowiedz == odp:
                        text = "Dobra Odpowiedz!"
                        color = (0, 255, 15)
                    else:
                        text = "Zla odpowiedz, poprawna to '"+odpowiedz+"'"
                        color = (255, 15, 0)
                        wynik = False
                        koniec = True
                    odp = ''

                    if self.runda > 13:
                        wynik = True
                        koniec = True


            display.blit(background_gra, (0, 0))
            display.blit(background_gra_prowadzoncy, (540, 0))
            pygame.draw.rect(display, (0, 153, 255), pygame.Rect(470, 600, 1000, 50), 2) #PYTANIA MOGĄ BYĆ NA MAKSYMALNIE 50 SŁÓW
            if wielkosc > 45: #haha nie
                display.blit(pytanie_font2.render(self.pytania[pytanie], True, white), [475, 610])
            else:
                display.blit(pytanie_font.render(self.pytania[pytanie], True, white), [475, 605])
            pygame.draw.rect(display, (0, 153, 255), pygame.Rect(455, 700, 500, 100), 2)
            pygame.draw.rect(display, (0, 153, 255), pygame.Rect(455, 850, 500, 100), 2)
            pygame.draw.rect(display, (0, 153, 255), pygame.Rect(985, 700, 500, 100), 2)
            pygame.draw.rect(display, (0, 153, 255), pygame.Rect(985, 850, 500, 100), 2)
            if wielkosc1 > 15:
                display.blit(odpowiedz_font2.render(self.odpowiedz_1[pytanie], True, white), [460, 728])
            else:
                display.blit(odpowiedz_font.render(self.odpowiedz_1[pytanie], True, white), [460, 725])
            if wielkosc2 > 15:
                display.blit(odpowiedz_font2.render(self.odpowiedz_2[pytanie], True, white), [460, 878])
            else:
                display.blit(odpowiedz_font.render(self.odpowiedz_2[pytanie], True, white), [460, 875])
            if wielkosc3 > 15:
                display.blit(odpowiedz_font2.render(self.odpowiedz_3[pytanie], True, white), [995, 728])
            else:
                display.blit(odpowiedz_font.render(self.odpowiedz_3[pytanie], True, white), [995, 725])
            if wielkosc4 > 15:
                display.blit(odpowiedz_font2.render(self.odpowiedz_4[pytanie], True, white), [995, 878])
            else:
                display.blit(odpowiedz_font.render(self.odpowiedz_4[pytanie], True, white), [995, 875])
            display.blit(odpowiedz_font.render("Runda: "+str(self.runda+1), True, white), [1550, 0])
            display.blit(odpowiedz_font.render("15.  1 000 000 zł", True, white), [1500, 100])
            display.blit(odpowiedz_font.render("14.  500 000 zł", True, orange), [1500, 150])
            display.blit(odpowiedz_font.render("13.  250 000 zł", True, orange), [1500, 200])
            display.blit(odpowiedz_font.render("12.  125 000 zł", True, orange), [1500, 250])
            display.blit(odpowiedz_font.render("11.  64 000 zł", True, orange), [1500, 300])
            display.blit(odpowiedz_font.render("10.  32 000 zł", True, white), [1500, 350])
            display.blit(odpowiedz_font.render("9.  16 000 zł", True, orange), [1510, 400])
            display.blit(odpowiedz_font.render("8.  8 000 zł", True, orange), [1510, 450])
            display.blit(odpowiedz_font.render("7.  4 000 zł", True, orange), [1510, 500])
            display.blit(odpowiedz_font.render("6.  2 000 zł", True, orange), [1510, 550])
            display.blit(odpowiedz_font.render("5.  1 000 zł", True, white), [1510, 600])
            display.blit(odpowiedz_font.render("4.  500 zł", True, orange), [1510, 650])
            display.blit(odpowiedz_font.render("3.  300 zł", True, orange), [1510, 700])
            display.blit(odpowiedz_font.render("2.  200 zł", True, orange), [1510, 750])
            display.blit(odpowiedz_font.render("1.  100 zł", True, orange), [1510, 800])
            pygame.draw.rect(display, (255, 253, 2), pygame.Rect(1499, 800 - (self.runda * 50), 415, 50), 4)
            pygame.draw.rect(display, (255, 53, 0), pygame.Rect(20, 50, 400, 150))
            display.blit(odpowiedz_font.render("Kliknij ESC by", True, white), [20, 75])
            display.blit(odpowiedz_font.render("wejść do menu", True, white), [20, 125])

            if self.debug_mode:
                display.blit(pytanie_font.render("Odp: "+self.prawidlowa_odpowiedz[pytanie], True, white), [10, 300])

            if Menu:
                if Opcja_w_menu > 0:
                    Opcja_w_menu = 0
                if Opcja_w_menu < -1:
                    Opcja_w_menu = -1
                pygame.gfxdraw.box(display, pygame.Rect(565, 200, 800, 600), (200, 200, 200,227))
                display.blit(odpowiedz_font.render("Menu", True, (255,255,0)), [570, 205])
                pygame.draw.rect(display, (255, 53, 0), pygame.Rect(765, 300, 400, 150))
                pygame.draw.rect(display, (255, 53, 0), pygame.Rect(765, 550, 400, 150))
                if Opcja_w_menu == 0:
                    pygame.draw.rect(display, black, pygame.Rect(765, 300, 400, 150),10)
                if Opcja_w_menu == -1:
                    pygame.draw.rect(display, black, pygame.Rect(765, 550, 400, 150),10)
                display.blit(odpowiedz_font.render("Wyjdz z gry", True, white), [770, 320])
                display.blit(odpowiedz_font.render("Przejdz do", True, white), [770, 570])
                display.blit(odpowiedz_font.render("kasyna", True, white), [770, 620])

            if animacja > 0:
                if animacja <= 1000:
                    pygame.draw.rect(display, color, pygame.Rect(470, 600, 1000, 50))
                    display.blit(pytanie_font.render(text, True, white), [500, 605])
                else:
                    pygame.gfxdraw.box(display, pygame.Rect(470, 600, 1000, 50), (255,255,0,(255-((animacja-1000)/2.8))))
                if animacja > 0 and animacja < 6:
                    if koniec:
                        self.pieniadze += wygrana_gotowka
                        #gotowka_fix += self.pieniadze
                        Zapisywanie_w_pliku().zapisz_dane_w_linicje("savefile.txt",str(int(Zapisywanie_w_pliku().odczytaj_podane_dane("savefile.txt",2))+self.pieniadze),1)
                        Milionerzy().koniec_gry(self.pieniadze,wynik,nazwa_gracza)
                        return False
                    if self.runda == 3:
                        wygrana_gotowka = 1000
                    if self.runda == 8:
                        wygrana_gotowka = 32000
                    if self.runda == 13:
                        wygrana_gotowka = 1000000
                    pytanie = random.randint(0, len(self.pytania) - 1)  # losujemy kolejne pytanie
                    wielkosc = len(self.pytania[pytanie])
                    wielkosc1 = len(self.odpowiedz_1[pytanie])
                    wielkosc2 = len(self.odpowiedz_2[pytanie])
                    wielkosc3 = len(self.odpowiedz_3[pytanie])
                    wielkosc4 = len(self.odpowiedz_4[pytanie])
                    self.runda += 1
            pygame.display.update()

    def koniec_gry(self,pieniadze,wynik,nazwa_gracza):
        self.runda = 0
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                mouse = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if (mouse[0] >= 551 and mouse[0] <= 1425) and (mouse[1] >= 690 and mouse[1] <= 789):
                        Milionerzy().gra(nazwa_gracza)
                        return False
            display.blit(background_gra, (0, 0))
            if wynik:
                display.blit(menu_font.render('Wygrałeś!!!!', True, (0, 255, 0)), [700, 450])
            else:
                display.blit(menu_font.render('Koniec gry naura', True, (255,0,0)), [600, 450])
            display.blit(menu_font.render('Wygrywasz: '+str(pieniadze)+'zł', True, white), [600, 550])
            pygame.draw.rect(display, white, pygame.Rect(550, 690, 875, 100), 5)
            pygame.gfxdraw.box(display, pygame.Rect(550, 690, 875, 100), (0, 0, 0, 127))
            display.blit(menu_font.render('Spróbuj jeszcze raz!', True, white), [555, 700])
            pygame.display.update()


pygame.init()
x = 1920
y = 1080
res = (x, y)
black = (0,0,0)
white = (255,255,255)
menu_text = (0,200,100)
orange = (255, 153, 51)
red = (240,0,0)
display = pygame.display.set_mode(res)
pygame.display.set_caption('Milionerzy')
icon = pygame.image.load('images/ikonka.png')
pygame.display.set_icon(icon)
background = pygame.image.load('images/tlo.png')
background_gra = pygame.image.load("images/tlo1.jpg")
background_gra_prowadzoncy = pygame.image.load("images/tlo2.jpg")
kasyno_menu_tlo = pygame.image.load("images/kasyno.jpg")
coinflip_tlo = pygame.image.load("images/tlo3.png")
pytania_txt = open("pytania.txt", "r")
savefile_txt = open("savefile.txt", "r+")
if Zapisywanie_w_pliku().czy_plik_posiada_dane("savefile.txt") == False: #trzeba to robić by zapisywanie działało
    savefile_txt.write('\n' + "0")
    savefile_txt.flush()
s = 0
prawidlowa_odpowiedz = []
pytania = []
odpowiedz_1 = []
odpowiedz_2 = []
odpowiedz_3 = []
odpowiedz_4 = []
for x in pytania_txt:  # system na odczytywanie pytan i odpowiedzi
    s += 1
    if s % 6 == 0:
        prawidlowa_odpowiedz.append(x[:-1])
    if s % 6 == 1:
        pytania.append(x[:-1])
    if s % 6 == 2:
        odpowiedz_1.append(x[:-1])
    if s % 6 == 3:
        odpowiedz_2.append(x[:-1])
    if s % 6 == 4:
        odpowiedz_3.append(x[:-1])
    if s % 6 == 5:
        odpowiedz_4.append(x[:-1])
menu_font = pygame.font.SysFont('Aria', 125)
menu_font2 = pygame.font.SysFont('Aria', 47)
pytanie_font = pygame.font.SysFont('Aria',55)
pytanie_font2 = pygame.font.SysFont('Aria',42)
odpowiedz_font = pygame.font.SysFont('Aria',80)
odpowiedz_font2 = pygame.font.SysFont('Aria',60)
def main():
    Milionerzy().menu()

if __name__ == '__main__':
    main()
